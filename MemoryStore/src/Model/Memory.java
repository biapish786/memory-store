package Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Memory{
private String title;
private String date;
private String location;
private String content;
private String photoAddres;


	public Memory() {
	super();
}
	public Memory(String title, String date, String location, String content,
		String photoAddres) {
	super();
	this.title = title;
	this.date = date;
	this.location = location;
	this.content = content;
	this.photoAddres = photoAddres;
}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPhotoAddres() {
		return photoAddres;
	}
	public void setPhotoAddres(String photoAddres) {
		this.photoAddres = photoAddres;
	}


}
