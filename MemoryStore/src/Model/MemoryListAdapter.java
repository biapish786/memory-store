package Model;

import java.util.ArrayList;

import com.example.memorystore.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SearchView;
import android.widget.TextView;

public class MemoryListAdapter extends BaseAdapter {

 Context context;
 ArrayList<Memory> memoryList;
 private ArrayAdapter<Memory> adapter;
 SearchView search;

 public MemoryListAdapter(Context context, ArrayList<Memory> list) {

  this.context = context;
  memoryList = list;
  
 }

 @Override
 public int getCount() {

  return memoryList.size();
 }

 @Override
 public Object getItem(int position) {

  return memoryList.get(position);
 }

 @Override
 public long getItemId(int position) {

  return position;
 }

 @Override
 public View getView(int position, View convertView, ViewGroup arg2) {
	 Memory memory = memoryList.get(position);

  if (convertView == null) {
   LayoutInflater inflater = (LayoutInflater) context
     .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
   convertView = inflater.inflate(R.layout.lists, null);

  }
 
  TextView dateView = (TextView) convertView.findViewById(R.id.txt_date);
  dateView.setText(memory.getDate());
  TextView titleView = (TextView) convertView.findViewById(R.id.txt_title);
  titleView.setText(memory.getTitle());

  return convertView;
 }

 
}
