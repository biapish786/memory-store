package com.example.memorystore;

import DB.SqlHandler;
import Model.Memory;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Change extends Activity{
	protected static final int RESULT_LOAD_IMAGE = 1;
	protected static final int PICK_IMAGE_REQUEST =  12345;
	SqlHandler db;
	Button create;
	Button selectPhoto;
	EditText title, date, location, content;
	private ImageView imageView;
	String picturePath;
	ListView listView;

	
	 protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.change);
	        insialize();
	        
	        
	        create.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Memory memory =new Memory();
					memory.setTitle(title.getText().toString());
					memory.setDate(date.getText().toString());
					memory.setLocation(location.getText().toString());
					memory.setContent(content.getText().toString());
					memory.setPhotoAddres(picturePath);
					
					 db.Update(memory);
					 
					
				}
			});
	        
	        
	        
	        selectPhoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				        photoPickerIntent.setType("image/*");
				        startActivityForResult(photoPickerIntent, PICK_IMAGE_REQUEST);

				}
			});

		}

	 public void insialize() {
			create = (Button) findViewById(R.id.btn_change_Create);
			selectPhoto = (Button) findViewById(R.id.btn_change_Create);
		
			title = (EditText) findViewById(R.id.change_title);
			date = (EditText) findViewById(R.id.change_date);
			location = (EditText) findViewById(R.id.change_location);
			content = (EditText) findViewById(R.id.change_content);
			imageView = (ImageView) findViewById(R.id.details_image);
			listView = (ListView) findViewById(R.id.listView1);
			db = new SqlHandler(getApplicationContext());
			
			
			
			
}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);

	        // Here we need to check if the activity that was triggers was the Image Gallery.
	        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
	        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
	        if (requestCode ==  PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {
	            // Let's read picked image data - its URI
	            Uri pickedImage = data.getData();
	            // Let's read picked image path using content resolver
	            String[] filePath = { MediaStore.Images.Media.DATA };
	            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
	            cursor.moveToFirst();
	            picturePath = cursor.getString(cursor.getColumnIndex(filePath[0]));


	            BitmapFactory.Options options = new BitmapFactory.Options();
	            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	            Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
	            imageView.setImageBitmap(bitmap);

	            // Do something with the bitmap

	            imageView.setVisibility(View.VISIBLE);
	            // At the end remember to close the cursor or you will end with the RuntimeException!
	            cursor.close();
	        }
	    }

}