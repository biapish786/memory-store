package com.example.memorystore;


import java.util.ArrayList;

import DB.SqlHandler;
import Model.Memory;
import Model.MemoryListAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;


public class MainActivity extends Activity {
Button btnAdd;
ListView listView;
SearchView search;
SqlHandler db;
Memory memory;
ArrayAdapter<Memory> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        insilize();
        adapter = new ArrayAdapter<Memory>(MainActivity.this,R.layout.activity_main);
        listView.setAdapter(adapter);
        
        ArrayList<Memory> list = db.getData();

    

 
		
	
     btnAdd.setOnClickListener(new View.OnClickListener() {
		
		public void onClick(View v) {
			Intent myIntent1 = new Intent(MainActivity.this,CreateMemory.class);
			startActivity(myIntent1);
		}
	});
     
     listView.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Intent myIntent1 = new Intent(MainActivity.this,Details.class);
			startActivity(myIntent1);
			
		}
	});
     listView.setOnLongClickListener(new OnLongClickListener() {
    	 
		
		private Context context;

		@Override
		public boolean onLongClick(View v) {
			
			memory = new Memory();
				AlertDialog builder = new AlertDialog.Builder(context).create();
				
				builder.setButton(AlertDialog.BUTTON_NEGATIVE,"NO",
						new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
				
				builder.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
						new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
							}
						});
				
				return false;
				
			
		}
	});
     
     
    }
    public void insilize() {
        
        btnAdd= (Button) findViewById(R.id.btnAdd);
        listView= (ListView) findViewById(R.id.listView1);
        search= (SearchView) findViewById(R.id.searchView1);
        db =  new SqlHandler(getApplicationContext());
        adapter= new ArrayAdapter<Memory>(getApplicationContext(), R.layout.activity_main);
        		
		
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        


       

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);
    }
   
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
