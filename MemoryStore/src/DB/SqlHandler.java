package DB;

import java.util.ArrayList;

import Model.Memory;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.SyncStateContract.Columns;

public class SqlHandler {

	public static final int DATABASE_VERSION = 2;
	Context context;
	static SQLiteDatabase sqlDatabase;
	static DBHelper dbHelper;

	public static final String DATABASE_NAME = "Memory.db";
	public static final String TABLE_NAME = "Memory_table";
	public static final String COL1 = "ID";
	public static final String Col2 = "Title";
	public static final String COL3 = "Date";
	public static final String COl4 = "Location";
	public static final String COL5 = "Content";
	public static final String COL6 = "PhotoAdress";

	private static final String[] COLUMNS = { COL1, Col2, COL3, COl4, COL5,
			COL6 };

	public SqlHandler(Context context) {

		dbHelper = new DBHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
		sqlDatabase = dbHelper.getWritableDatabase();
	}

	public void executeQuery(String query) {
		try {

			if (sqlDatabase.isOpen()) {
				sqlDatabase.close();
			}

			sqlDatabase = dbHelper.getWritableDatabase();
			sqlDatabase.execSQL(query);

		} catch (Exception e) {

			System.out.println("DATABASE ERROR " + e);
		}

	}

	public long AddData(Memory memory) {
		long id;

		ContentValues cv = fillContentVAlues(memory);

		id = sqlDatabase.insert(TABLE_NAME, null, cv);
dbHelper.close();
		return id;

	}
public int Update(Memory memory){


//	String query = "SELECT * FROM Memory_table ";
	ContentValues cv = fillContentVAlues(memory);
	int c1 = sqlDatabase.update(TABLE_NAME, cv, null, null);





	
	dbHelper.close();
	return c1;

}

	
	

public long Delete(Memory memory){
	long id;

	ContentValues cv = fillContentVAlues(memory);

	id = sqlDatabase.delete(TABLE_NAME, null,null);
	
	
			dbHelper.close();
	return id;
	
	
	
}
	private ContentValues fillContentVAlues(Memory memory) {
		// TODO Auto-generated method stub

		ContentValues cv = new ContentValues();

		cv.put(COLUMNS[1], memory.getTitle());
		cv.put(COLUMNS[2], memory.getDate());
		cv.put(COLUMNS[3], memory.getLocation());
		cv.put(COLUMNS[4], memory.getContent());
		cv.put(COLUMNS[5], memory.getPhotoAddres());

		return cv;
	}
	
	public ArrayList<Memory> getData() {

		ArrayList<Memory> memoryList = new ArrayList<Memory>();

//		String query = "SELECT * FROM Memory_table ";

		Cursor c1 = sqlDatabase.query(TABLE_NAME, COLUMNS , null, null, null, null, null);

		while (c1.moveToNext()) {
			
			Memory memory = new Memory();

			memory.setTitle(c1.getString(c1.getColumnIndex(COLUMNS[1])));
			memory.setDate(c1.getString(c1.getColumnIndex(COLUMNS[2])));

			memoryList.add(memory);

		}

		c1.close();
		dbHelper.close();
		return memoryList;

	}
	
		
		
	}
